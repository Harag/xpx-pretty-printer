(defsystem "xpx-pretty-printer"
  :description "This is an CLtL2 compatible version of the November, 26 1991 version of Richard C. Waters' XP pretty printer."
  :version "2020.7.24"
  :author "Phil Marneweck"
  :licence "MIT from the origninal source."
  :depends-on ()
  :components ((:file "package")
	       (:file "xp-code" :depends-on ("package"))
               


))

