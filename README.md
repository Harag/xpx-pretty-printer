# xpx-pretty-printer

This is a project to realign Richard C. Waters' XP pretty printer to be a portable pretty printer for ansi cl again, what ever that means.

To see the original code as is go to https://gitlab.com/Harag/xp-pretty-printer

The original license:
```
;------------------------------------------------------------------------
;Copyright Massachusetts Institute of Technology, Cambridge, Massachusetts.
;Permission to use, copy, modify, and distribute this software and its
;documentation for any purpose and without fee is hereby granted,
;provided that this copyright and permission notice appear in all
;copies and supporting documentation, and that the name of M.I.T. not
;be used in advertising or publicity pertaining to distribution of the
;software without specific, written prior permission. M.I.T. makes no
;representations about the suitability of this software for any
;purpose.  It is provided "as is" without express or implied warranty.
;    M.I.T. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
;    ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
;    M.I.T. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
;    ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
;    WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
;    ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
;    SOFTWARE.
;------------------------------------------------------------------------
```

## Towards a portable pretty printer 

XP was designed to be a portable pretty printing library for use by different implementation, but then it became part of the standard and it does not look like it was updated to conform to the standard again.

So if I want it to be truely portable then I need to work through the following and slowly but surely ammend XP.

The standard
http://www.lispworks.com/documentation/lw50/CLHS/Body/22_b.htm

The original
https://dspace.mit.edu/bitstream/handle/1721.1/6503/AIM-1102.pdf

Some of the thinking behind pretty-printing in the standard
http://www.lispworks.com/documentation/HyperSpec/Issues/iss270_w.htm


## State of things:

So far you can load and use it, but some of the tests cause heap exaustion but the sbcl fails at the same test so I need to get a deaper understanding of what the tests are testing (maybe the impossible).

