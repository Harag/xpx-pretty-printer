(in-package :common-lisp-user)

(defpackage :xp
  (:use :cl)
  (:shadow #:*print-pprint-dispatch*
	   #:*print-right-margin*
	   #:*print-miser-width*
	   #:*print-lines*
	   #:pprint-dispatch
	   #:copy-pprint-dispatch
	   #:set-pprint-dispatch
	   #:pprint-fill
	   #:pprint-linear
	   #:pprint-tabular
	   #:pprint-logical-block
	   #:pprint-pop
	   #:pprint-exit-if-list-exhausted
	   #:pprint-newline
	   #:pprint-indent
	   #:pprint-tab
	   #:formatter
           
	   #:write
	   #:print
	   #:prin1
	   #:princ
	   #:pprint
	   #:format
	   #:write-to-string
	   #:princ-to-string
	   #:prin1-to-string
	   #:write-line
	   #:write-string
	   #:write-char
	   #:terpri
	   #:fresh-line
	   #:defstruct
	   #:finish-output
	   #:force-output
	   #:clear-output)
  (:export

   :*print-shared*
   :enable-format-control-read-macro   
   :formatter
   :copy-pprint-dispatch
   :pprint-dispatch
   :set-pprint-dispatch
   :pprint-fill
   :pprint-linear
   :pprint-tabular
   :pprint-logical-block
   :pprint-pop
   :pprint-exit-if-list-exhausted
   :pprint-newline
   :pprint-indent
   :pprint-tab
   :*print-pprint-dispatch*
   :*print-right-margin*
   :*default-right-margin*
   :*print-miser-width*
   :*print-lines*
   :*last-abbreviated-printing*

   :write
   :print
   :prin1
   :princ
   :pprint
   :format
   :write-to-string
   :princ-to-string
   :prin1-to-string
   :write-line
   :write-string
   :write-char
   :terpri
   :fresh-line
   :defstruct
   :finish-output
   :force-output
   :clear-output
   
   ))
